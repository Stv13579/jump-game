﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathScreen : MonoBehaviour
{
    public static DeathScreen DeathScreenInstance { get; private set; }
    public GameObject deathMenuUI;

    public Jumper player;

    private void Awake()
    {
        DeathScreenInstance = this;
    }

    public void Respawn()
    {
        deathMenuUI.SetActive(false);
        Time.timeScale = 1f;
        //Load At Checkpoint or last passed area
        player.Respawn();
        Debug.Log("Respawn");
    }

    void Pause()
    {
        deathMenuUI.SetActive(true);
        Time.timeScale = 0f;
    }

    public void LoadMenu()
    {
        SceneManager.LoadScene("MAINMENU");
    }

    public void QuitGame()
    {
        Application.Quit();
        Debug.Log("Quit");
    }
}
