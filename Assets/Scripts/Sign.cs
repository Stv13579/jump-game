﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Sign : MonoBehaviour
{

    public GameObject dialogueBox;
    public Text dialogueText;
    [TextArea(3, 10)]
    public string dialogue;
    public bool playerInrange;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Player"))
        {
            playerInrange = true;
            dialogueBox.SetActive(true);
            dialogueText.text = dialogue;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if(other.CompareTag("Player"))
        {
            playerInrange = false;
            dialogueBox.SetActive(false);
        }
    }
}
