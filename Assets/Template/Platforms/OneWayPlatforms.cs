﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneWayPlatforms : MonoBehaviour
{

    private BoxCollider2D Collision;
    // Start is called before the first frame update
    void Start()
    {
        Collision = GetComponent<BoxCollider2D>();
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.S))
        {
            Collision.isTrigger = true;
        }
        else
        {
            Collision.isTrigger = false;
        }
    }
}
