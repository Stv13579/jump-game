﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveGameObject : MonoBehaviour
{
    //variables
    [Header("Object to move and movement speed")]
    [SerializeField] GameObject gameObject;
    public float moveSpeed;

    [Header("Waypoints for objects to follow")]
    [SerializeField] Transform[] waypoints;
    [SerializeField] int firstWaypoint;

    [Header("Movement options")]
    [SerializeField] bool useReverse = false;
    [SerializeField] bool useFlip = false;
    public bool FacingRight = false;
    [SerializeField] SpriteRenderer spriteRenderer;
    //[SerializeField] Animator animator;

    Transform nextWaypoint;
    bool reverse;


    // Start is called before the first frame update
    void Start()
    {
        nextWaypoint = waypoints[firstWaypoint];
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, nextWaypoint.position, Time.deltaTime * moveSpeed);

        if (reverse)
        {
            Reverse();
        }
        else
        {
            Forward();
        }
    }

    //user functions
    void Forward()
    {
        if (gameObject.transform.position == nextWaypoint.position)
        {
            firstWaypoint++;
            if (useFlip) Flip();

            if (useReverse)
            {
                reverse = true;
                firstWaypoint = waypoints.Length - 1;
            }
            else
            {
                firstWaypoint = 0;
            }
            nextWaypoint = waypoints[firstWaypoint];
        }
    }

    void Reverse()
    {
        if (gameObject.transform.position == nextWaypoint.position)
        {
            firstWaypoint--;

            if (firstWaypoint == 0)
            {
                reverse = false;
                firstWaypoint = 0;
            }
            nextWaypoint = waypoints[firstWaypoint];
        }
    }

    void Flip()
    {
        FacingRight = !FacingRight;

        if (FacingRight)
        {
            spriteRenderer.flipX = true;
            //animator.SetBool("FacingRight", true);
        }
        else
        {
            spriteRenderer.flipX = false;
            //animator.SetBool("FacingRight", false);
        }
    }
 }

