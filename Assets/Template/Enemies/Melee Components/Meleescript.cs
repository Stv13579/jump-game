﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meleescript : MonoBehaviour
{
    public GameObject player;
    Vector3 playerPosition;
    public GameObject attack;
    public bool attacking = false;
    public int health;
    public Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Jumper");
        playerPosition = player.transform.position;
    }
    void playerCheck()
    {
        if (!attacking)
        {
            if (GetComponent<MoveGameObject>().FacingRight)
            {
                if ((player.transform.position.x - transform.position.x) > 0 && (player.transform.position.x - transform.position.x) < 1.5)
                {
                    if (((player.transform.position.y - transform.position.y) > -1 && (player.transform.position.y - transform.position.y) < 1))
                    {
                        attacking = true;
                        GameObject hit = Instantiate(attack, transform.position + new Vector3(0.1f, 0, 0), transform.rotation);
                        hit.GetComponent<AttackScript>().enemy = this.gameObject;
                        hit.transform.parent = this.gameObject.transform;
                        GetComponent<MoveGameObject>().moveSpeed = 0;

                    }
                }
            }
            else
            {
                if ((player.transform.position.x - transform.position.x) < 0 && (player.transform.position.x - transform.position.x) > -1.5)
                {
                    if (((player.transform.position.y - transform.position.y) > -1 && (player.transform.position.y - transform.position.y) < 1))
                    {
                        attacking = true;
                        GameObject hit = Instantiate(attack, transform.position - new Vector3(0.1f, 0, 0), transform.rotation);
                        hit.GetComponent<AttackScript>().enemy = this.gameObject;
                        hit.transform.parent = this.gameObject.transform;
                        GetComponent<MoveGameObject>().moveSpeed = 0;



                    }
                }
            }

            if (attacking == true)
            {
                anim.SetBool("isAttacking", true);
            }
            else
            {
                anim.SetBool("isAttacking", false);
            }
        }
        


    }

    // Update is called once per frame
    void Update()
    {
        playerCheck();
        if (health <= 0)
        {
            Destroy(this.gameObject);
        }
    }

}
