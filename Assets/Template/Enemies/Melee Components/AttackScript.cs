﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackScript : MonoBehaviour
{
    public float time = 0.2f;
    public GameObject enemy;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
       if (time > 0)
       {
            time -= Time.deltaTime;
       }
       else
       {
            enemy.GetComponent<Meleescript>().attacking = false;
            enemy.GetComponent<MoveGameObject>().moveSpeed = 2;

            Destroy(this.gameObject);
       }
    }
}
