﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedScript : MonoBehaviour
{
    public GameObject player;
    Vector3 playerPosition;
    public GameObject attack;
    public bool attacking = false;
    public int health;
    public float attackx;
    public float attacky;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Jumper");
        playerPosition = player.transform.position;
    }
    void playerCheck()
    {
        if (!attacking)
        {


            if (GetComponent<MoveGameObject>().FacingRight)
            {
                if ((player.transform.position.x - transform.position.x) > 0 && (player.transform.position.x - transform.position.x) < attackx)
                {
                    if (((player.transform.position.y - transform.position.y) > -attacky && (player.transform.position.y - transform.position.y) < attacky))
                    {
                        attacking = true;
                        GameObject hit = Instantiate(attack, transform.position + new Vector3(0.1f, 0, 0), transform.rotation);
                        hit.GetComponent<RangedAttackScript>().enemy = this.gameObject;
                        hit.transform.parent = this.gameObject.transform;

                    }
                }
            }
            else
            {
                if ((player.transform.position.x - transform.position.x) < 0 && (player.transform.position.x - transform.position.x) > -attackx)
                {
                    if (((player.transform.position.y - transform.position.y) > -attacky && (player.transform.position.y - transform.position.y) < attacky))
                    {
                        attacking = true;
                        GameObject hit = Instantiate(attack, transform.position - new Vector3(0.1f, 0, 0), transform.rotation);
                        hit.GetComponent<RangedAttackScript>().enemy = this.gameObject;
                        hit.transform.parent = this.gameObject.transform;



                    }
                }
            }
        }



    }

    // Update is called once per frame
    void Update()
    {
        if (player.transform.position.x > transform.position.x)
        {
            GetComponent<MoveGameObject>().FacingRight = true;
        }
        else
        {
            GetComponent<MoveGameObject>().FacingRight = false;
        }
        playerCheck();
        if (health <= 0)
        {
            Destroy(this.gameObject);
        }
    }
}
