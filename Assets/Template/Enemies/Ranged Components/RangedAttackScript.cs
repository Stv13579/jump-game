﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedAttackScript : MonoBehaviour
{
    public GameObject enemy;
    public GameObject player;
    public int damage;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Jumper");

        Vector3 playerPos = player.transform.position;
        playerPos.z = 0.0f;
        Vector3 targetDir = (playerPos - transform.position).normalized;
        transform.rotation = Quaternion.LookRotation(Vector3.forward, playerPos - transform.position);
        GetComponent<Rigidbody2D>().velocity = new Vector2(targetDir.x, targetDir.y) * 10;

    }

    // Update is called once per frame
    void Update()
    {
        if ((transform.position - enemy.transform.position).magnitude > 20.0f)
        {
            enemy.GetComponent<RangedScript>().attacking = false;
            Destroy(this.gameObject);

        }

    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject != enemy)
        {
            if (collision.gameObject == player)
            {
                player.GetComponent<Jumper>().Damage(damage);
            }
            enemy.GetComponent<RangedScript>().attacking = false;
            Destroy(this.gameObject);
        }
    }
}
