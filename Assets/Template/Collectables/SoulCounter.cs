﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoulsCounter : MonoBehaviour
{
    public Text SoulsText;
    public int intSouls;

    // Update is called once per frame
    void Update()
    {
        SoulsText.text = ": " + intSouls;
    }
}
