﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_checkpoint : MonoBehaviour
{
    private Jumper player;

    public Transform checkpointPos;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Jumper>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Player"))
        {
            player.setSpawn(checkpointPos);
        }
    }
}
