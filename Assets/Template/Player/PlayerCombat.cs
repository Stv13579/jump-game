﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCombat : MonoBehaviour
{
    //animator
    //SFX
    public AudioSource hit1;
    public AudioSource hit2;
    public AudioSource hit3;
    public AudioSource hit4;


    public Transform attackPoint;
    public float attackRange = 0.5f;
    public LayerMask enemyLayers;

    private int comboCounter;
    private float comboCooldown;
    private float attackCooldown;


    void Start()
    {
        comboCounter = 0;
        comboCooldown = 0;
        attackCooldown = 0;
        enemyLayers = 1 << 10;
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(1, 1, 0, 0.75F);
        Gizmos.DrawSphere(attackPoint.position, attackRange);
    }


    void DamageAllEnemies(int damage)
    {
        // play attack animation
        Debug.Log("attacked");
        //detect all enemies in range of attack
        Collider2D[] hitEnemies = Physics2D.OverlapCircleAll(attackPoint.position, attackRange, enemyLayers);

        //Damage all enemies  in range
        foreach (Collider2D enemy in hitEnemies)
        {
            if (enemy.gameObject.GetComponent<Meleescript>() != null)
            {
                enemy.gameObject.GetComponent<Meleescript>().health -= damage;
                enemy.gameObject.GetComponent<Flash>().flashTime = 0.2f;
                switch (comboCounter)
                {
                    case 0:
                        CinemachineShake.instance.ShakeCamera(1.0f, 0.2f);
                        hit1.Play();
                        break;
                    case 1:
                        CinemachineShake.instance.ShakeCamera(1.0f, 0.2f);
                        hit2.Play();
                        break;
                    case 2:
                        CinemachineShake.instance.ShakeCamera(1.0f, 0.2f);
                        hit3.Play();
                        break;
                    case 3:
                        CinemachineShake.instance.ShakeCamera(1.5f, 0.2f);
                        hit4.Play();
                        break;

                    default:
                        break;
                }
                comboCounter++;
            }

            if (enemy.gameObject.GetComponent<RangedScript>() != null)
            {
                enemy.gameObject.GetComponent<RangedScript>().health -= damage;
                enemy.gameObject.GetComponent<Flash>().flashTime = 0.2f;
                switch (comboCounter)
                {
                    case 0:
                        CinemachineShake.instance.ShakeCamera(1.0f, 0.2f);
                        hit1.Play();
                        break;
                    case 1:
                        CinemachineShake.instance.ShakeCamera(1.0f, 0.2f);
                        hit2.Play();
                        break;
                    case 2:
                        CinemachineShake.instance.ShakeCamera(1.0f, 0.2f);
                        hit3.Play();
                        break;
                    case 3:
                        CinemachineShake.instance.ShakeCamera(1.5f, 0.2f);
                        hit4.Play();
                        break;

                    default:
                        break;
                }
                comboCounter++;
            }
            
            Debug.Log("hit enemy");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (comboCounter > 3)
            {//reset combo hit;
                comboCounter = 0;
            }
            if (comboCooldown < 0)
            {//reset combo timer
                comboCounter = 0;
            }

            if (attackCooldown <= 0)
            {
                switch (comboCounter)
                {
                    case 0:
                        DamageAllEnemies(1);
                        attackCooldown = 0.25f;
                        comboCooldown = 2f;
                        break;
                    case 1:
                        DamageAllEnemies(1);
                        attackCooldown = 0.25f;
                        comboCooldown = 2f;
                        break;
                    case 2:
                        DamageAllEnemies(1);
                        attackCooldown = 0.25f;
                        comboCooldown = 2f;
                        break;
                    case 3:
                        DamageAllEnemies(2);
                        attackCooldown = 1.0f;
                        comboCooldown = 2f;
                        break;

                    default:
                        break;
                }
            }
            //diaply attack

        }

        if (Input.GetKeyDown(KeyCode.A))
        {
            attackPoint.localPosition = new Vector2(-1, 0);
        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            attackPoint.localPosition = new Vector2(1, 0);
        }

        comboCooldown -= Time.deltaTime;
        attackCooldown -= Time.deltaTime;
    }
}
