﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Staff : MonoBehaviour
{

    public GameObject player;

    bool fired = false;

    public bool attached = false;

    public bool returning = false;

    public int shootSpeed = 12;

    public int returnSpeed = 20;

    RaycastHit2D hit;

    int layermask;

    public AudioSource enemyHit;
    public AudioSource Throw;
    public AudioSource ReturnThrow;
    public AudioSource Stick;



    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Jumper");
        gameObject.GetComponent<BoxCollider2D>().enabled = false;
        Physics2D.IgnoreCollision(player.gameObject.GetComponent<Collider2D>(), GetComponent<Collider2D>());
        // points staff towards mouse
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.rotation = Quaternion.LookRotation(Vector3.forward, mousePos - transform.position);
        transform.position = player.transform.position;
        layermask = 1 << 8;
        Debug.Log(layermask);
        this.gameObject.GetComponent<BoxCollider2D>().size = new Vector2(this.gameObject.GetComponent<BoxCollider2D>().size.x, 10);
        this.gameObject.GetComponent<BoxCollider2D>().offset = new Vector2(this.gameObject.GetComponent<BoxCollider2D>().offset.x, 10);
        //layermask = ~layermask;

    }

    // Update is called once per frame
    void Update()
    {
        if (!fired && !returning)
        {
            // points staff towards mouse
            float camToPlayerDist = Vector3.Distance(transform.position, Camera.main.transform.position);
            Vector2 mouseWorldPosition = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, camToPlayerDist));
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 direction = mouseWorldPosition - (Vector2)transform.position;
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(0, 0, angle - 90);
            transform.position = new Vector2(player.transform.position.x, player.transform.position.y + 0.15f);
            if (Input.GetMouseButtonDown(1))
            {
                Throw.Play();
                Vector3 targetPos = Camera.main.ScreenToWorldPoint(mousePos);
                targetPos.z = transform.position.z;
                Vector3 targetDir = (targetPos - transform.position).normalized;
                RaycastHit2D hit = Physics2D.Raycast(transform.position, targetDir, 5, 8);
                if (hit.collider == null)
                {
                    shootStaff();
                    gameObject.GetComponent<BoxCollider2D>().enabled = true;
                }

            }
        }
        else
        {
            //staff is fired
            if (Input.GetMouseButtonDown(1))
            {
                ReturnThrow.Play();
                returning = true;
                gameObject.GetComponent<BoxCollider2D>().enabled = false;
            }
            if (attached)
            {
                if (player.transform.position.y <= (transform.position.y + 0.1) && ((Mathf.Abs(transform.rotation.eulerAngles.z - 270) < 1) || (Mathf.Abs(transform.rotation.eulerAngles.z - 90) < 1)))
                {
                    Physics2D.IgnoreCollision(player.gameObject.GetComponent<Collider2D>(), GetComponent<Collider2D>());
                }
                else
                {
                    Physics2D.IgnoreCollision(player.gameObject.GetComponent<Collider2D>(), GetComponent<Collider2D>(), false);
                }
            }
        }
           //staff returns to player
        if (returning)
        {
            returnStaff();
            if ((transform.position - player.transform.position).magnitude <= 2.2)
            {
                returning = false;
                gameObject.GetComponent<BoxCollider2D>().enabled = false;
            }
        }
        if ((transform.position - player.transform.position).magnitude > 20.0f)
        {
            ReturnThrow.Play();
            returning = true;
            gameObject.GetComponent<BoxCollider2D>().enabled = false;

        }

    }

    void shootStaff()
    {
        if (!fired)
        {
            gameObject.layer = 0;
            Vector3 mousePos = Input.mousePosition;
            mousePos.z = 0.0f;
            Vector3 targetPos = Camera.main.ScreenToWorldPoint(mousePos);
            targetPos.z = transform.position.z;
            Vector3 targetDir = (targetPos - transform.position).normalized;
            GetComponent<Rigidbody2D>().velocity = new Vector2(targetDir.x, targetDir.y) * shootSpeed;
            GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
            fired = true;
        }
    }

    void returnStaff()
    {
        attached = false;
        fired = false;
        gameObject.layer = 0;
        this.gameObject.GetComponent<BoxCollider2D>().size = new Vector2(this.gameObject.GetComponent<BoxCollider2D>().size.x, 10);
        this.gameObject.GetComponent<BoxCollider2D>().offset = new Vector2(this.gameObject.GetComponent<BoxCollider2D>().offset.x, 10);
        GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
        Physics2D.IgnoreCollision(player.GetComponent<Collider2D>(), GetComponent<Collider2D>());
        Vector3 playerPos = player.transform.position;
        playerPos.z = 0.0f;
        Vector3 targetDir = (playerPos - transform.position).normalized;
        transform.rotation = Quaternion.LookRotation(Vector3.forward, playerPos - transform.position);
        GetComponent<Rigidbody2D>().velocity = new Vector2(targetDir.x, targetDir.y) * returnSpeed;
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player" && (!attached || !fired))
        {
            Physics2D.IgnoreCollision(collision.gameObject.GetComponent<Collider2D>(), GetComponent<Collider2D>());
        }
        else if (collision.gameObject.layer == 10 && !attached)
        {
            if (collision.gameObject.GetComponent<Meleescript>() != null)
            {
                collision.gameObject.GetComponent<Meleescript>().health -= 1;
                collision.gameObject.GetComponent<Flash>().flashTime = 0.2f;
                CinemachineShake.instance.ShakeCamera(1.0f, 0.2f);
                enemyHit.Play();   
            }

            if (collision.gameObject.GetComponent<RangedScript>() != null)
            {
                collision.gameObject.GetComponent<RangedScript>().health -= 1;
                collision.gameObject.GetComponent<Flash>().flashTime = 0.2f;
                CinemachineShake.instance.ShakeCamera(1.0f, 0.2f);
                enemyHit.Play();
            }
            returning = true;
            gameObject.GetComponent<BoxCollider2D>().enabled = false;
        }
        else if (collision.gameObject.tag == "Projectile" && !attached)
        {
            returning = true;
            gameObject.GetComponent<BoxCollider2D>().enabled = false;
        }
        else if ((collision.gameObject.layer == 10))
        {

        }
        else if (collision.gameObject.tag == "Projectile")
        {

        }
        else if (collision.gameObject.tag == "Player")
        {

        }
        else
        {
            Stick.Play();
            GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            attached = true;
            gameObject.layer = 8;
            GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
            Physics2D.IgnoreCollision(player.GetComponent<Collider2D>(), GetComponent<Collider2D>(), false);
            ContactPoint2D contact = collision.contacts[0];
            Vector3 pos = contact.point;
            this.gameObject.GetComponent<BoxCollider2D>().size = new Vector2(this.gameObject.GetComponent<BoxCollider2D>().size.x, 20);
            this.gameObject.GetComponent<BoxCollider2D>().offset = new Vector2(this.gameObject.GetComponent<BoxCollider2D>().offset.x, 0);


            hit = Physics2D.Raycast(transform.position, transform.up, Mathf.Infinity, 1 << 8);
            if (hit.collider != null)
            {
                if (hit.normal.x == 1.0)
                {
                    this.transform.eulerAngles = new Vector3(0, 0, 90.0f);
                    this.transform.position = new Vector2(pos.x + 1.3f, pos.y);
                }
                else if (hit.normal.x == -1.0)
                {
                    this.transform.eulerAngles = new Vector3(0, 0, -90.0f);
                    this.transform.position = new Vector2(pos.x - 1.3f, pos.y);
                }
                else if (hit.normal.y == 1.0)
                {
                    this.transform.eulerAngles = new Vector3(0, 0, 180f);
                    this.transform.position = new Vector2(pos.x, pos.y + 1.3f);

                }
                else if (hit.normal.y == -1.0)
                {
                    this.transform.eulerAngles = new Vector3(0, 0, 0f);
                    this.transform.position = new Vector2(pos.x, pos.y - 1.3f);

                }
                else
                {
                    this.transform.eulerAngles = new Vector3(0.0f, 0.0f, 90 * Mathf.Round((transform.rotation.eulerAngles.z / 90)));
                    if ((Mathf.Abs(transform.rotation.eulerAngles.z - 270) < 1) || (Mathf.Abs(transform.rotation.eulerAngles.z - 90) < 1))
                    {
                        transform.position = new Vector2(transform.position.x, pos.y);
                    }
                    else
                    {
                        transform.position = new Vector2(pos.x, transform.position.y);
                    }
                }
                


            }


        }
    }

    bool getAttached()
    {
        return attached;
    }
}
