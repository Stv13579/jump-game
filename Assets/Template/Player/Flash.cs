﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flash : MonoBehaviour
{
    private int flashCount = 30;
    public float flashTime;
    //flashes when hit

    // Update is called once per frame
    void Update()
    {
        if (flashTime > 0)
        {
            this.GetComponent<SpriteRenderer>().color = Color.red;
        }
        else
        {
            this.GetComponent<SpriteRenderer>().color = Color.white;
        }
        flashTime -= Time.deltaTime;
    }
}
