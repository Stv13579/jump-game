﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Jumper : MonoBehaviour
{
    public float damageInvulnerablility;
    private float damageInvuln;
    //player speed
    public float speed;

    public float jumpVelocity; // <--- set to 25
    public float fallMultiplier;

    private Rigidbody2D rb;
    private float moveInput;

    private bool isGrounded;
    public Transform feetPos;

    public float checkRadius;
    public LayerMask whatIsGround;

    private float jumpTimeCounter;
    public float jumpTime;
    private bool isJumping;

    public float HoverForce; // <--- set to 2000

    public GameObject staff;

    //player health
    public int health;
    public int numOfHearts;

    public Image[] hearts;
    public Sprite fullHeart;
    public Sprite emptyHeart;

    public bool PlayerIsDead;

    public Vector2 spawnPoint;

    public AudioSource hurt;

    private int flashCount = 60;
    private bool bFlash = true;

    //
    void flash()
    {
        flashCount--;
        if (flashCount > 0)
        {
            if (bFlash)
            {
                this.GetComponent<SpriteRenderer>().color = Color.red;
            }
            else
            {
                this.GetComponent<SpriteRenderer>().color = Color.white;
            }
        }
        else
        {
            bFlash = !bFlash;
            flashCount = 60;
        }
    }

    // Use for initialization
    void Start()
    {
        rb = this.GetComponent<Rigidbody2D>();
        staff = GameObject.Find("Staff");
        isJumping = false;
        damageInvuln = 0;

        spawnPoint.x = 0;
        spawnPoint.y = 0;
    }

    void Update()
    {
        //display for hearts
        if (health > numOfHearts)
        {
            health = numOfHearts;
        }

        for (int i = 0; i < hearts.Length; i++)
        {
            if (i < health)
            {
                hearts[i].sprite = fullHeart;
            }
            else
            {
                hearts[i].sprite = emptyHeart;
            }

            if (i < numOfHearts)
            {
                hearts[i].enabled = true;
            }
            else
            {
                hearts[i].enabled = false;
            }
        }

        if (health <= 0)
        {
            DeathScreen.DeathScreenInstance.deathMenuUI.SetActive(true);
            CinemachineShake.instance.ShakeCamera(0f, 1f);
            Time.timeScale = 0.0f;
        }

        if (damageInvuln > 0)
        {
            flash();
        }
        else
        {
            this.GetComponent<SpriteRenderer>().color = Color.white;
        }
    }

    void FixedUpdate()
    {
        //sets players current grounded state based on if circle radius check is collidiong with ground
        isGrounded = Physics2D.OverlapCircle(feetPos.position, checkRadius, whatIsGround);

        //increased gravity once player's downward velocity is > 0
        if (rb.velocity.y < 0)
        {
            //floating mechanic, adds upward force if space is held down. 
            if (Input.GetKey(KeyCode.Space))
            {
                rb.AddForce(Vector2.up * HoverForce * Time.fixedDeltaTime);
            }
            else
            {//increases gravity if falling
                rb.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier) * Time.fixedDeltaTime;
            }
        }
        else
        {//sets gravity to normal
            rb.velocity += Vector2.up * Physics2D.gravity.y * (1) * Time.fixedDeltaTime;
        }

        //player jumps if is on ground and space bar is pressed
        if (isGrounded == true && !isJumping && Input.GetKey(KeyCode.Space))
        {
            isGrounded = false;
            isJumping = true;
            jumpTimeCounter = jumpTime;
            rb.velocity = Vector2.up * jumpVelocity;
        }

        //adds extra foce when space is held down 
        if (Input.GetKey(KeyCode.Space) && isJumping == true)
        {
            if (jumpTimeCounter > 0)
            {
                rb.AddForce(Vector2.up * jumpVelocity);
                jumpTimeCounter -= Time.fixedDeltaTime;
            }
            else
            {
                isJumping = false;
            }
        }
        //if no key input set jumping to false so the player cant activate the extra boost after lifiting up the spacebar.
        if (!Input.GetKey(KeyCode.Space))
        {
            isJumping = false;
        }

        //player movement left and right
        moveInput = Input.GetAxis("Horizontal");
        rb.velocity = new Vector2(moveInput * speed, rb.velocity.y);

        // damage invulderablitiy
        if (damageInvuln >= 0.0f)
        {
            damageInvuln -= Time.fixedDeltaTime;
        }
    }

    //fuction is called by other objects that can damage the player
    public void Damage(int _iDamage)
    {
        if (damageInvuln <= 0)
        {
            hurt.Play();
            health -= _iDamage;
            CinemachineShake.instance.ShakeCamera(3.0f, 0.2f);
            damageInvuln = damageInvulnerablility;
        }
    }

    public void Respawn()
    {
        Time.timeScale = 1.0f;
        health = 5;
        rb.position = spawnPoint;
    }

    public void setSpawn(Transform _tTransform)
    {
        spawnPoint.x = _tTransform.position.x;
        spawnPoint.y = _tTransform.position.y;
    }
}