﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SignTrigger : MonoBehaviour
{
    public Dialogue dialogue;
   // public DialogueTrigger trigger;

    public float checkRadius;
    public LayerMask whatIsPlayer;

    private bool isInRange;

    public Transform TriggerPos;

    // Update is called once per frame
    void Update()
    {
        isInRange = Physics2D.OverlapCircle(TriggerPos.position, checkRadius, whatIsPlayer);

        if (isInRange == true)
        {
            //display press e to interact
            if (Input.GetKey(KeyCode.E))
            {
                FindObjectOfType<DialogueManager>().StartDialogue(dialogue);
            }
        }
    }
}
